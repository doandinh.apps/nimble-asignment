package com.example.domain.model.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("grant_type")
    val grantType: String,

    val email: String,
    val password: String,

) : BaseRequest()