package com.example.domain.model.request

import com.google.gson.annotations.SerializedName

data class ForgotPasswordRequest(
    val user: User,

) : BaseRequest()

data class User(
    val email: String
)
