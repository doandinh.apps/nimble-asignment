package com.example.domain.repository

import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.UserModel
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.LoginRequest
import com.example.domain.model.request.RefreshTokenRequest


interface UserRepository {
    suspend fun login(query: LoginRequest): Result<UserModel>
    suspend fun refreshToken(query: RefreshTokenRequest): Result<UserModel>
    suspend fun forgotPassword(query: ForgotPasswordRequest): Result<ForgotPasswordModel>
}