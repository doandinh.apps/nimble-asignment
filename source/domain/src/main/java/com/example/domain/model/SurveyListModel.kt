package com.example.domain.model

import java.io.Serializable

data class SurveyListModel(
    val items: List<Item>,
    val page: Int,
    val pages: Int,

    val pageSize: Int,

    val records: Int,
    var fromRefresh: Boolean = false,
    var fromCache: Boolean = false

) {
    data class Item(
        val id: String,
        val title: String,
        val description: String,

        val coverImageURL: String,
    ) : Serializable

}
