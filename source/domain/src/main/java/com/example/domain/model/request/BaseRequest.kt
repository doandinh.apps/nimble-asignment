package com.example.domain.model.request

import com.google.gson.annotations.SerializedName

open class BaseRequest {
    @SerializedName("client_id")
    var clientID: String = ""

    @SerializedName("client_secret")
    var clientSecret: String = ""
}