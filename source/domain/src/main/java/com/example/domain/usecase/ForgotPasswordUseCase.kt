package com.example.domain.usecase

import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.repository.UserRepository

class ForgotPasswordUseCase(private val user: UserRepository) {

    suspend fun execute(query: ForgotPasswordRequest): Result<ForgotPasswordModel> {
        return user.forgotPassword(query)
    }

}
