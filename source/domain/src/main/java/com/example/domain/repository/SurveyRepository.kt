package com.example.domain.repository

import com.example.domain.Result
import com.example.domain.model.SurveyListModel

interface SurveyRepository {
    suspend fun getSurveyList(page: Int, size: Int): Result<SurveyListModel>
    suspend fun getLocalSurveyList(page: Int, size: Int): Result<SurveyListModel>
    suspend fun resetCache()
}