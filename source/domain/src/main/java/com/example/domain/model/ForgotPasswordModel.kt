package com.example.domain.model

data class ForgotPasswordModel(
    val message: String
)
