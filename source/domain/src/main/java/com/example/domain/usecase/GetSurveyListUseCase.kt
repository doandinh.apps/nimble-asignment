package com.example.domain.usecase

import com.example.domain.Result
import com.example.domain.model.SurveyListModel
import com.example.domain.repository.SurveyRepository

class GetSurveyListUseCase(private val surveyRepo: SurveyRepository) {

    suspend fun execute(page: Int, size: Int): Result<SurveyListModel> {
        return surveyRepo.getSurveyList(page, size)
    }

    suspend fun executeFromLocal(page: Int, size: Int): Result<SurveyListModel> {
        return surveyRepo.getLocalSurveyList(page, size)
    }

    suspend fun resetCache() {
        surveyRepo.resetCache()
    }

}
