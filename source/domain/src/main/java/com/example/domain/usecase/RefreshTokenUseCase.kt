package com.example.domain.usecase

import com.example.domain.Result
import com.example.domain.model.UserModel
import com.example.domain.model.request.RefreshTokenRequest
import com.example.domain.repository.UserRepository

class RefreshTokenUseCase(private val user: UserRepository) {

    suspend fun execute(query: RefreshTokenRequest): Result<UserModel> {
        return user.refreshToken(query)
    }

}
