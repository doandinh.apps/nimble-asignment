package com.example.domain.usecase

import com.example.domain.Result
import com.example.domain.model.UserModel
import com.example.domain.model.request.LoginRequest
import com.example.domain.repository.UserRepository


class LoginUseCase(private val userRepository: UserRepository) {

    suspend fun execute(query: LoginRequest): Result<UserModel> {
        return userRepository.login(query)
    }

}
