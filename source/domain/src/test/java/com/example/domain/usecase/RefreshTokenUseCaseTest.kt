package com.example.domain.usecase

import com.example.domain.Result
import com.example.domain.model.UserModel
import com.example.domain.model.request.RefreshTokenRequest
import com.example.domain.repository.UserRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RefreshTokenUseCaseTest {

    private lateinit var refreshTokenUseCase: RefreshTokenUseCase

    @Mock
    private lateinit var resultMocK: UserModel

    @Mock
    private lateinit var userRepository: UserRepository


    @Before
    fun setUp() {
        refreshTokenUseCase = RefreshTokenUseCase(
            userRepository
        )
    }

    @Test
    fun testRefreshTokenUseCaseObservableHappyCase() {
        val request = RefreshTokenRequest(
            grantType = "test",
            refreshToken = "test1",
            "clientIdTest",
            "secret"
        )
        runBlocking {
            `when`(userRepository.refreshToken(request)).thenReturn(Result.Success(resultMocK))
            refreshTokenUseCase.execute(request)
            verify(userRepository).refreshToken(request)
        }

    }

}
