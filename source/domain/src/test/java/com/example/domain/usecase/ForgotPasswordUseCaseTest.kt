package com.example.domain.usecase

import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.User
import com.example.domain.repository.UserRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ForgotPasswordUseCaseTest {

    private lateinit var forgotPasswordUseCase: ForgotPasswordUseCase

    @Mock
    private lateinit var resultMocK: ForgotPasswordModel

    @Mock
    private lateinit var userRepository: UserRepository


    @Before
    fun setUp() {
        forgotPasswordUseCase = ForgotPasswordUseCase(
            userRepository
        )
    }

    @Test
    fun testForgotPasswordUseCaseObservableHappyCase() {
        val user = User("test@email.com")
        val request = ForgotPasswordRequest(user, "clientId", "clientSecret")
        runBlocking {
            `when`(userRepository.forgotPassword(request)).thenReturn(Result.Success(resultMocK))
            forgotPasswordUseCase.execute(request)
            verify(userRepository).forgotPassword(request)
        }

    }

}
