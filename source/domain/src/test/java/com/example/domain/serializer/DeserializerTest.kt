package com.example.domain.serializer

import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.LoginRequest
import com.example.domain.model.request.RefreshTokenRequest
import com.example.domain.model.request.User
import com.google.gson.Gson
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DeserializerTest {

    lateinit var gson: Gson

    @Before
    fun setUp() {
        gson = Gson()
    }


    @Test
    fun testLoginRequest() {
        val request = LoginRequest(
            grantType = "password",
            email = "dev@nimblehq.co",
            password = "12345678",
            clientID = "123",
            clientSecret = "456"
        )
        val jsonRequest = gson.toJson(request)
        assertTrue(jsonRequest.isNotEmpty())
        val request2 = gson.fromJson(jsonRequest, LoginRequest::class.java)
        assertNotNull(request2)

        assertTrue(request2.grantType == request.grantType)
        assertTrue(request2.email == request.email)
        assertTrue(request2.password == request.password)
        assertTrue(request2.clientID == request.clientID)
        assertTrue(request2.clientSecret == request.clientSecret)
    }

    @Test
    fun testRefreshTokenRequest() {
        val request = RefreshTokenRequest(
            grantType = "refresh_token",
            refreshToken = "t9KR8HoiJXZfdfs5vVB1PKwjg7m7ynvXBrUkpUyxYkU",
            clientID = "123",
            clientSecret = "456"
        )
        val jsonRequest = gson.toJson(request)
        assertTrue(jsonRequest.isNotEmpty())
        val request2 = gson.fromJson(jsonRequest, RefreshTokenRequest::class.java)
        assertNotNull(request2)

        assertTrue(request2.grantType == request.grantType)
        assertTrue(request2.refreshToken == request.refreshToken)
        assertTrue(request2.clientID == request.clientID)
        assertTrue(request2.clientSecret == request.clientSecret)

    }

    @Test
    fun testForgotPasswordRequest() {
        val user = User("test@gmail.com")
        val request = ForgotPasswordRequest(user = user, clientID = "123", clientSecret = "456")
        val jsonRequest = gson.toJson(request)
        assertTrue(jsonRequest.isNotEmpty())
        val request2 = gson.fromJson(jsonRequest, ForgotPasswordRequest::class.java)
        assertNotNull(request2)
        assertNotNull(request2.user)
        assertTrue(request2.user.email == request.user.email)
        assertTrue(request2.clientID == request.clientID)
        assertTrue(request2.clientSecret == request.clientSecret)


    }
}