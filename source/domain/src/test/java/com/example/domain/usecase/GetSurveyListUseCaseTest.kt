package com.example.domain.usecase


import com.example.domain.Result
import com.example.domain.model.SurveyListModel
import com.example.domain.repository.SurveyRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetSurveyListUseCaseTest {

    private lateinit var getSurveyListUseCase: GetSurveyListUseCase

    @Mock
    private lateinit var resultMocK: SurveyListModel

    @Mock
    private lateinit var surveyRepository: SurveyRepository


    @Before
    fun setUp() {
        getSurveyListUseCase = GetSurveyListUseCase(
            surveyRepository
        )
    }

    @Test
    fun testGetSurveyListUseCaseObservableHappyCase() {
        val page = 1
        val size = 2
        runBlocking {
            `when`(
                surveyRepository.getSurveyList(
                    page,
                    size
                )
            ).thenReturn(Result.Success(resultMocK))
            getSurveyListUseCase.execute(page, size)
            verify(surveyRepository).getSurveyList(page, size)
        }

    }

}
