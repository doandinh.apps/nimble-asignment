package com.example.domain.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class ForgotPasswordModelTest {


    private lateinit var forgotPasswordModel: ForgotPasswordModel

    @Before
    fun setUp() {
        forgotPasswordModel = ForgotPasswordModel(
            message = "testMessage"
        )
    }

    @Test
    fun testForgotPasswordConstructorHappyCase() {
        assertThat(forgotPasswordModel.message).isEqualTo("testMessage")
    }
}
