package com.example.domain.usecase

import com.example.domain.Result
import com.example.domain.model.UserModel
import com.example.domain.model.request.LoginRequest
import com.example.domain.repository.UserRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginUseCaseTest {

    private lateinit var loginUseCase: LoginUseCase

    @Mock
    private lateinit var resultMocK: UserModel

    @Mock
    private lateinit var userRepository: UserRepository


    @Before
    fun setUp() {
        loginUseCase = LoginUseCase(
            userRepository
        )
    }

    @Test
    fun testLoginUseCaseObservableHappyCase() {
        val requestLogin = LoginRequest("test1", "test2", "passtest", "test 3", "test4")
        runBlocking {
            `when`(userRepository.login(requestLogin)).thenReturn(Result.Success(resultMocK))
            loginUseCase.execute(requestLogin)
            verify(userRepository).login(requestLogin)
        }

    }

}
