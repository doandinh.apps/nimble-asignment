package com.example.domain.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class UserModelTest {


    private lateinit var userModel: UserModel

    @Before
    fun setUp() {
        userModel = UserModel(
            "testToken",
            "testType",
            2000,
            "testRefreshToken",
            3000,
        )
    }

    @Test
    fun testUserConstructorHappyCase() {

        assertThat(userModel.accessToken).isEqualTo("testToken")
        assertThat(userModel.tokenType).isEqualTo("testType")
        assertThat(userModel.expiresIn).isEqualTo(2000)
        assertThat(userModel.refreshToken).isEqualTo("testRefreshToken")
        assertThat(userModel.createdAt).isEqualTo(3000)
    }
}
