package com.example.domain.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class SurveyListModelTest {
    private lateinit var surveyListModel: SurveyListModel

    @Before
    fun setUp() {
        val testItemModel = SurveyListModel.Item("id", "itemTitle", "itemDescription", "itemImageUrl")
        val testListItemModel = arrayListOf(testItemModel)
        surveyListModel = SurveyListModel(
            items = testListItemModel,
            page = 1,
            pages = 2,
            pageSize = 10,
            records = 20
        )
    }

    @Test
    fun testSurveyListModelConstructorHappyCase() {
        assertThat(surveyListModel.items).isNotNull
        assertThat(surveyListModel.items.size).isEqualTo(1)

        assertThat(surveyListModel.page).isEqualTo(1)
        assertThat(surveyListModel.pages).isEqualTo(2)
        assertThat(surveyListModel.pageSize).isEqualTo(10)
        assertThat(surveyListModel.records).isEqualTo(20)
        //item test
        val item = surveyListModel.items[0]
        assertThat(item.title).isEqualTo("itemTitle")
        assertThat(item.description).isEqualTo("itemDescription")
        assertThat(item.coverImageURL).isEqualTo("itemImageUrl")
    }
}
