package com.example.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.entity.SurveyListEntity

@Dao
interface SurveysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(newsList: List<SurveyListEntity.SurveyListEntityDatum>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(news: SurveyListEntity.SurveyListEntityDatum)

    @Query("SELECT * FROM SurveyListEntityDatum ORDER BY sortKey ASC LIMIT :limit OFFSET :offset ")
    suspend fun getPagedSurvey(
        offset: Int,
        limit: Int
    ): List<SurveyListEntity.SurveyListEntityDatum>

    @Query("SELECT COUNT(id) FROM SurveyListEntityDatum ")
    suspend fun getCount(): Int
}
