package com.example.data.api


import com.example.data.entity.SurveyListEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface AppService {


    @GET("/api/v1/surveys")
    suspend fun getSurveyList(
        @Query("page[number]") page: Int,
        @Query("page[size]") size: Int
    ): SurveyListEntity

}
