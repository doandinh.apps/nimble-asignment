package com.example.data.entity

data class ForgotPasswordEntity(
    val meta: Meta
) {
    data class Meta(
        val message: String
    )
}


