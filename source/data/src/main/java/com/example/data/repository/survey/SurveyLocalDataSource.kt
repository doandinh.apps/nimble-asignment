package com.example.data.repository.survey

import com.example.data.AppDatabase
import com.example.data.mapper.toSurveyListModel
import com.example.domain.Result
import com.example.domain.model.SurveyListModel

class SurveyLocalDataSource(private val db: AppDatabase) : SurveyDataSource {


    override suspend fun getSurveyList(page: Int, size: Int): Result<SurveyListModel> {
        return try {
            val offset = (page - 1) * size
            val result = db.getSurveyListDao().getPagedSurvey(offset, size)
            Result.Success(result.toSurveyListModel())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun resetCache() {
        db.clearAllTables()
    }
}