package com.example.data.repository.user

import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.UserModel
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.LoginRequest
import com.example.domain.model.request.RefreshTokenRequest
import com.example.domain.repository.UserRepository

class UserRepositoryImpl constructor(
    private val userRemote: UserDataSource
) : UserRepository {
    override suspend fun login(query: LoginRequest): Result<UserModel> {
        return userRemote.login(query)
    }

    override suspend fun refreshToken(query: RefreshTokenRequest): Result<UserModel> {
        return userRemote.refreshToken(query)
    }

    override suspend fun forgotPassword(query: ForgotPasswordRequest): Result<ForgotPasswordModel> {
        return userRemote.forgotPassword(query)
    }


}