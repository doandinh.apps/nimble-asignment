package com.example.data.repository.survey

import com.example.data.AppDatabase
import com.example.data.api.AppService
import com.example.data.mapper.toSurveyListModel
import com.example.domain.Result
import com.example.domain.model.SurveyListModel

class SurveyRemoteDataSource(private val serviceApi: AppService, private val db: AppDatabase) :
    SurveyDataSource {


    override suspend fun getSurveyList(page: Int, size: Int): Result<SurveyListModel> {
        return try {
            val result = serviceApi.getSurveyList(page, size)
            val count = (page - 1) * size
            for (i in result.data.indices) {
                result.data[i].sortKey = count + i
            }
            db.getSurveyListDao().insertAll(result.data)
            Result.Success(result.toSurveyListModel())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun resetCache() {

    }
}