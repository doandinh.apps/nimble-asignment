package com.example.data.repository.user

import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.UserModel
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.LoginRequest
import com.example.domain.model.request.RefreshTokenRequest

interface UserDataSource {
    suspend fun login(query: LoginRequest): Result<UserModel>
    suspend fun forgotPassword(query: ForgotPasswordRequest): Result<ForgotPasswordModel>
    suspend fun refreshToken(query: RefreshTokenRequest): Result<UserModel>

}