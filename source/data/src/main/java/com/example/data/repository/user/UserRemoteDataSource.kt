package com.example.data.repository.user

import com.example.data.BuildConfig
import com.example.data.api.AuthenService
import com.example.data.mapper.toForgotPasswordModel
import com.example.data.mapper.toUserModel
import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.UserModel
import com.example.domain.model.request.BaseRequest
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.LoginRequest
import com.example.domain.model.request.RefreshTokenRequest

class UserRemoteDataSource(private val serviceApi: AuthenService) : UserDataSource {


    override suspend fun login(query: LoginRequest): Result<UserModel> {
        return try {
            val result = serviceApi.loginEmail(addKeyToRequest(query))
            Result.Success(result.toUserModel())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun forgotPassword(query: ForgotPasswordRequest): Result<ForgotPasswordModel> {
        return try {
            val result = serviceApi.forgotPassword(addKeyToRequest(query))
            Result.Success(result.toForgotPasswordModel())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun refreshToken(query: RefreshTokenRequest): Result<UserModel> {
        return try {
            val result = serviceApi.refreshToken(addKeyToRequest(query))
            Result.Success(result.toUserModel())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <K> addKeyToRequest(request : BaseRequest) : K {
        return request.apply {
            clientID = BuildConfig.CLIENT_ID
            clientSecret = BuildConfig.CLIENT_SECRET
        } as K
    }
}