package com.example.data.repository.survey

import com.example.domain.Result
import com.example.domain.model.SurveyListModel

interface SurveyDataSource {

    suspend fun getSurveyList(page: Int, size: Int): Result<SurveyListModel>

    suspend fun resetCache()

}