package com.example.data.entity

import androidx.room.*
import com.google.gson.annotations.SerializedName

data class SurveyListEntity(
    val data: List<SurveyListEntityDatum>,
    val meta: Meta
) {

    @Entity
    data class SurveyListEntityDatum(
        var sortKey: Int,
        @PrimaryKey
        var id: String,
        var type: String?,
        @Embedded var attributes: Attributes?,
        @Ignore var relationships: Relationships?
    ) {
        constructor() : this(0, "", "", null, null)

        data class Attributes(
            @ColumnInfo(name = "title")
            var title: String?,
            @ColumnInfo(name = "description")
            var description: String?,

            @SerializedName("thank_email_above_threshold")
            var thankEmailAboveThreshold: String?,

            @SerializedName("thank_email_below_threshold")
            var thankEmailBelowThreshold: String?,

            @SerializedName("is_active")
            var isActive: Boolean?,

            @ColumnInfo(name = "cover_image_url")
            @SerializedName("cover_image_url")
            var coverImageURL: String?,

            @SerializedName("created_at")
            var createdAt: String?,

            @SerializedName("active_at")
            var activeAt: String?,


            @SerializedName("survey_type")
            var surveyType: String?
        )

        data class Relationships(
            val questions: Questions
        ) {
            data class Questions(
                val data: List<QuestionsDatum>
            ) {
                data class QuestionsDatum(
                    val id: String,
                    val type: String
                )
            }
        }
    }

    data class Meta(
        val page: Int,
        val pages: Int,

        @SerializedName("page_size")
        val pageSize: Int,

        val records: Int
    )
}


