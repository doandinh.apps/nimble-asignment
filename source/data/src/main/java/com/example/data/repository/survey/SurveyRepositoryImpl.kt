package com.example.data.repository.survey

import com.example.domain.Result
import com.example.domain.model.SurveyListModel
import com.example.domain.repository.SurveyRepository

class SurveyRepositoryImpl constructor(
    private val surveyRemote: SurveyDataSource,
    private val surveyLocal: SurveyLocalDataSource

) : SurveyRepository {
    override suspend fun getSurveyList(page: Int, size: Int): Result<SurveyListModel> {
        return surveyRemote.getSurveyList(page, size)
    }

    override suspend fun getLocalSurveyList(page: Int, size: Int): Result<SurveyListModel> {
        return surveyLocal.getSurveyList(page, size)
    }

    override suspend fun resetCache() {
        surveyLocal.resetCache()
    }


}