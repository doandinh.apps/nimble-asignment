package com.example.data.api


import com.example.data.entity.ForgotPasswordEntity
import com.example.data.entity.UserEntity
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.LoginRequest
import com.example.domain.model.request.RefreshTokenRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenService {

    @POST("/api/v1/oauth/token")
    suspend fun loginEmail(@Body request: LoginRequest): UserEntity

    @POST("/api/v1/oauth/token")
    suspend fun refreshToken(@Body request: RefreshTokenRequest): UserEntity

    @POST("/api/v1/passwords")
    suspend fun forgotPassword(@Body request: ForgotPasswordRequest): ForgotPasswordEntity


}
