package com.example.data.mapper

import com.example.data.entity.ForgotPasswordEntity
import com.example.data.entity.SurveyListEntity
import com.example.data.entity.UserEntity
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.SurveyListModel
import com.example.domain.model.UserModel

fun ForgotPasswordEntity.toForgotPasswordModel() = ForgotPasswordModel(
    message = meta.message
)

fun SurveyListEntity.toSurveyListModel() = SurveyListModel(
    page = meta.page,
    pages = meta.pages,
    pageSize = meta.pageSize,
    records = meta.records,
    items = data.map { origin ->
        SurveyListModel.Item(
            id = origin.id,
            title = origin.attributes?.title ?: "",
            description = origin.attributes?.description ?: "",
            coverImageURL = origin.attributes?.coverImageURL ?: "",
        )
    }
)

fun List<SurveyListEntity.SurveyListEntityDatum>.toSurveyListModel() = SurveyListModel(
    page = 1,
    pages = 1,
    pageSize = 1,
    records = 1,
    items = map { origin ->
        SurveyListModel.Item(
            id = origin.id,
            title = origin.attributes?.title ?: "",
            description = origin.attributes?.description ?: "",
            coverImageURL = origin.attributes?.coverImageURL ?: "",
        )
    }
)

fun UserEntity.toUserModel() = UserModel(
    accessToken = data.attributes.accessToken,

    tokenType = data.attributes.tokenType,

    expiresIn = data.attributes.expiresIn,

    refreshToken = data.attributes.refreshToken,

    createdAt = data.attributes.createdAt,
)