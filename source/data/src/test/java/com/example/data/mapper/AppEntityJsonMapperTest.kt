package com.example.data.mapper

import com.example.data.entity.ForgotPasswordEntity
import com.example.data.entity.SurveyListEntity
import com.example.data.entity.UserEntity
import com.example.data.serializer.JSON_FORGOT_PWD_ENTITY_TEST
import com.example.data.serializer.JSON_SURVEY_LIST_TEST
import com.example.data.serializer.JSON_USER_ENTITY_TEST
import com.google.gson.Gson
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(org.mockito.junit.MockitoJUnitRunner::class)
class AppEntityJsonMapperTest {

    lateinit var surveyListEntity: SurveyListEntity
    lateinit var userEntity: UserEntity
    lateinit var forgotPasswordEntity: ForgotPasswordEntity

    @Before
    fun setUp() {
        val gson = Gson()
        surveyListEntity = gson.fromJson(JSON_SURVEY_LIST_TEST, SurveyListEntity::class.java)
        userEntity = gson.fromJson(JSON_USER_ENTITY_TEST, UserEntity::class.java)
        forgotPasswordEntity =
            gson.fromJson(JSON_FORGOT_PWD_ENTITY_TEST, ForgotPasswordEntity::class.java)
    }


    @Test
    fun testTransformUserEntityHappyCase() {
        val resultModel = userEntity.toUserModel()
        Assert.assertThat(
            resultModel.accessToken,
            `is`("lbxD2K2BjbYtNzz8xjvh2FvSKx838KBCf79q773kq2c")
        )
        Assert.assertThat(resultModel.tokenType, `is`("Bearer"))
        Assert.assertThat(resultModel.expiresIn, `is`(7200))
        Assert.assertThat(
            resultModel.refreshToken,
            `is`("3zJz2oW0njxlj_I3ghyUBF7ZfdQKYXd2n0ODlMkAjHc")
        )
        Assert.assertThat(resultModel.createdAt, `is`(1597169495))

    }

    @Test
    fun testTransformSurveyListEntityHappyCase() {
        val resultModel = surveyListEntity.toSurveyListModel()
        Assert.assertThat(resultModel.page, `is`(1))
        Assert.assertThat(
            resultModel.pages,
            `is`(10)
        )
        Assert.assertThat(resultModel.pageSize, `is`(2))
        Assert.assertThat(resultModel.records, `is`(20))
        Assert.assertNotNull(resultModel.items)
        Assert.assertTrue(resultModel.items.isNotEmpty())

        val itemOne = resultModel.items[0]
        Assert.assertThat(itemOne.title, `is`("Scarlett Bangkok"))
        Assert.assertThat(itemOne.description, `is`("We'd love ot hear from you!"))
        Assert.assertThat(
            itemOne.coverImageURL,
            `is`("https://dhdbhh0jsld0o.cloudfront.net/m/1ea51560991bcb7d00d0_")
        )

    }

    @Test
    fun testTransformForgotPassEntityHappyCase() {
        val resultModel = forgotPasswordEntity.toForgotPasswordModel()
        Assert.assertThat(
            resultModel.message,
            `is`("If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.")
        )

    }

}
