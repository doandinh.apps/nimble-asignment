package com.example.data.repository

import com.example.data.AppDatabase
import com.example.data.api.AppService
import com.example.data.entity.SurveyListEntity
import com.example.data.repository.survey.SurveyRemoteDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CloudDataSurveyTest {

    private lateinit var cloudDataStore: SurveyRemoteDataSource

    @Mock
    lateinit var mockRestApi: AppService

    @Mock
    lateinit var dataBase: AppDatabase

    @Before
    fun setUp() {
        cloudDataStore = SurveyRemoteDataSource(mockRestApi, dataBase)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testGetSurveyListFromApi() {
        val page = 1
        val size = 2
        val surveyListEntity = mock(SurveyListEntity::class.java)

        runBlockingTest {
            given(mockRestApi.getSurveyList(page, size)).willReturn(surveyListEntity)
            cloudDataStore.getSurveyList(page, size)
            verify(mockRestApi).getSurveyList(page, size)
        }

    }


}
