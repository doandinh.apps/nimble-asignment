package com.example.data.serializer


import com.example.data.entity.ForgotPasswordEntity
import com.example.data.entity.SurveyListEntity
import com.example.data.entity.UserEntity
import com.google.gson.Gson
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SerializerTest {

    lateinit var gson: Gson

    @Before
    fun setUp() {
        gson = Gson()
    }

    @get: Rule
    val expectedException: ExpectedException = ExpectedException.none()

    @Test
    fun testSerializeForgotPassEntityHappyCase() {
        val resultEntity =
            gson.fromJson(JSON_FORGOT_PWD_ENTITY_TEST, ForgotPasswordEntity::class.java)
        //meta test
        assertNotNull(resultEntity.meta)
        assertTrue(resultEntity.meta.message.isNotEmpty())
        assertThat(
            resultEntity.meta.message,
            `is`("If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.")
        )
    }

    @Test
    fun testSerializeUserEntityHappyCase() {
        val userEntity = gson.fromJson(JSON_USER_ENTITY_TEST, UserEntity::class.java)
        //data test
        assertNotNull(userEntity.data)
        assertTrue(userEntity.data.id == 10L)
        assertTrue(userEntity.data.type == "token")
        //attributes test
        assertNotNull(userEntity.data.attributes)
        assertTrue(userEntity.data.attributes.accessToken == "lbxD2K2BjbYtNzz8xjvh2FvSKx838KBCf79q773kq2c")
        assertTrue(userEntity.data.attributes.tokenType == "Bearer")
        assertTrue(userEntity.data.attributes.expiresIn == 7200L)
        assertTrue(userEntity.data.attributes.refreshToken == "3zJz2oW0njxlj_I3ghyUBF7ZfdQKYXd2n0ODlMkAjHc")
        assertTrue(userEntity.data.attributes.createdAt == 1597169495L)

    }

    @Test
    fun testSerializeSurveyListEntityHappyCase() {
        val resultEntity = gson.fromJson(JSON_SURVEY_LIST_TEST, SurveyListEntity::class.java)
        assertNotNull(resultEntity.data)
        assertTrue(resultEntity.data.isNotEmpty())
        //item test
        val firstItem = resultEntity.data[0]
        //attributes test
        assertNotNull(firstItem.attributes)
        assertTrue(firstItem.attributes?.title == "Scarlett Bangkok")
        assertTrue(firstItem.attributes?.description == "We'd love ot hear from you!")
        assertTrue(firstItem.attributes?.thankEmailAboveThreshold == "Test1")
        assertTrue(firstItem.attributes?.thankEmailBelowThreshold == "Test2")
        assertTrue(firstItem.attributes?.isActive == true)
        assertTrue(firstItem.attributes?.coverImageURL == "https://dhdbhh0jsld0o.cloudfront.net/m/1ea51560991bcb7d00d0_")
        assertTrue(firstItem.attributes?.createdAt == "2017-01-23T07:48:12.991Z")
        assertTrue(firstItem.attributes?.activeAt == "2015-10-08T07:04:00.000Z")
        assertTrue(firstItem.attributes?.surveyType == "Restaurant")

        //relationship test
        assertNotNull(firstItem.relationships)
        assertNotNull(firstItem.relationships?.questions)
        assertNotNull(firstItem.relationships?.questions?.data)
        assertTrue(firstItem.relationships?.questions?.data?.isNotEmpty() ?: false)
        //first data test
        val firstData = firstItem.relationships?.questions?.data?.first()
        assertTrue(firstData?.id == "d3afbcf2b1d60af845dc")
        assertTrue(firstData?.type == "question")

        //meta test
        assertNotNull(resultEntity.meta)
        assertTrue(resultEntity.meta.page == 1)
        assertTrue(resultEntity.meta.pages == 10)
        assertTrue(resultEntity.meta.pageSize == 2)
        assertTrue(resultEntity.meta.records == 20)
    }


}
