package com.example.data.repository

import com.example.data.api.AuthenService
import com.example.data.entity.ForgotPasswordEntity
import com.example.data.entity.UserEntity
import com.example.data.repository.user.UserRemoteDataSource
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.LoginRequest
import com.example.domain.model.request.RefreshTokenRequest
import com.example.domain.model.request.User
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CloudDataUserTest {

    private lateinit var cloudDataStore: UserRemoteDataSource

    @Mock
    lateinit var mockRestApi: AuthenService

    @Before
    fun setUp() {
        cloudDataStore = UserRemoteDataSource(mockRestApi)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testLogin() {
        val request = LoginRequest("type", "email", "pwd")
        val userEntity = mock(UserEntity::class.java)
        runBlockingTest {
            given(mockRestApi.loginEmail(request)).willReturn(userEntity)
            cloudDataStore.login(request)
            verify(mockRestApi).loginEmail(request)
        }

    }

    @ExperimentalCoroutinesApi
    @Test
    fun testRefreshToken() {
        val request = RefreshTokenRequest("type", "token")
        val userEntity = mock(UserEntity::class.java)

        runBlockingTest {
            given(mockRestApi.refreshToken(request)).willReturn(userEntity)
            cloudDataStore.refreshToken(request)
            verify(mockRestApi).refreshToken(request)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testForgotPassword() {
        val request = ForgotPasswordRequest(user = User("email"))
        val forgotPassEntity = mock(ForgotPasswordEntity::class.java)

        runBlockingTest {
            given(mockRestApi.forgotPassword(request)).willReturn(forgotPassEntity)
            cloudDataStore.forgotPassword(request)
            verify(mockRestApi).forgotPassword(request)
        }
    }

}
