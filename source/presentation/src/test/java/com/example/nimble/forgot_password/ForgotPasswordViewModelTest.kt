package com.example.nimble.forgot_password

import androidx.lifecycle.Observer
import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.User
import com.example.domain.usecase.ForgotPasswordUseCase
import com.example.nimble.base.BaseViewModelTest
import com.example.nimble.rules.runBlockingTest
import com.example.nimble.viewmodel.ForgotPasswordViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ForgotPasswordViewModelTest : BaseViewModelTest() {

    @Mock
    lateinit var forgotPasswordUseCase: ForgotPasswordUseCase

    private lateinit var viewModel: ForgotPasswordViewModel

    @Before
    fun setUp() {
        viewModel =
                ForgotPasswordViewModel(forgotPasswordUseCase, coroutineRule.testDispatcherProvider)
    }

    @Test
    fun forgotPassword_onSuccess_hideLoadingAndShowResult() {
        coroutineRule.runBlockingTest {

            val resultObs: Observer<ForgotPasswordModel> = mock()
            val showLoadingObs: Observer<Boolean> = mock()
            val request = ForgotPasswordRequest(User("email"))

            viewModel.getResult().observeForever(resultObs)
            viewModel.getLoadingLiveData().observeForever(showLoadingObs)

            Mockito.`when`(forgotPasswordUseCase.execute(request))
                    .thenReturn(Result.Success(mock()))

            viewModel.submit("email")

            val inOrder = Mockito.inOrder(showLoadingObs, showLoadingObs, resultObs)

            inOrder.verify(showLoadingObs).onChanged(true)
            inOrder.verify(showLoadingObs).onChanged(false)
            inOrder.verify(resultObs).onChanged(any())
        }
    }

    @Test
    fun forgotPassword_onFailure_hideLoadingAndShowErrorMessage() {
        coroutineRule.runBlockingTest {

            val resultObs: Observer<ForgotPasswordModel> = mock()
            val errorObs: Observer<Throwable> = mock()
            val showLoadingObs: Observer<Boolean> = mock()
            val request = ForgotPasswordRequest(User("email"))

            viewModel.getResult().observeForever(resultObs)
            viewModel.getShowErrorLiveData().observeForever(errorObs)
            viewModel.getLoadingLiveData().observeForever(showLoadingObs)

            Mockito.`when`(forgotPasswordUseCase.execute(request)).thenReturn(Result.Error(mock()))

            viewModel.submit("email")

            val inOrder = Mockito.inOrder(showLoadingObs, errorObs)

            inOrder.verify(showLoadingObs).onChanged(true)
            inOrder.verify(errorObs).onChanged(any())
            Mockito.verifyZeroInteractions(resultObs)
        }
    }

    @Test
    fun forgotPassword_onLoading_showLoadingView() {
        coroutineRule.runBlockingTest {

            val showLoadingObs: Observer<Boolean> = mock()

            viewModel.getLoadingLiveData().observeForever(showLoadingObs)


            viewModel.submit("test")

            Mockito.verify(showLoadingObs).onChanged(true)
        }
    }


}