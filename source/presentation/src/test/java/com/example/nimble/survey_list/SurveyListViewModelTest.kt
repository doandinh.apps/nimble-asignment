package com.example.nimble.survey_list

import androidx.lifecycle.Observer
import com.example.domain.Result
import com.example.domain.model.SurveyListModel
import com.example.domain.usecase.GetSurveyListUseCase
import com.example.nimble.base.BaseViewModelTest
import com.example.nimble.rules.runBlockingTest
import com.example.nimble.viewmodel.SurveyViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SurveyListViewModelTest : BaseViewModelTest() {

    @Mock
    lateinit var surveyListUseCase: GetSurveyListUseCase

    private lateinit var viewModel: SurveyViewModel

    @Before
    fun setUp() {
        viewModel = SurveyViewModel(surveyListUseCase, coroutineRule.testDispatcherProvider)
    }

    @Test
    fun getSurveyList_onSuccess_hideLoadingAndShowResult() {
        coroutineRule.runBlockingTest {

            val resultObs: Observer<SurveyListModel> = mock()
            val showLoadingObs: Observer<Boolean> = mock()

            viewModel.onDataLoaded().observeForever(resultObs)
            viewModel.getLoadingLiveData().observeForever(showLoadingObs)

            Mockito.`when`(surveyListUseCase.execute(1, 5)).thenReturn(Result.Success(mock()))

            viewModel.loadSurvey()

            val inOrder = Mockito.inOrder(showLoadingObs, showLoadingObs, resultObs)

            inOrder.verify(showLoadingObs).onChanged(true)
            inOrder.verify(showLoadingObs).onChanged(false)
            inOrder.verify(resultObs).onChanged(any())
        }
    }

    @Test
    fun getSurveyList_onFailure_hideLoadingAndShowErrorMessage() {
        coroutineRule.runBlockingTest {

            val resultObs: Observer<SurveyListModel> = mock()
            val errorObs: Observer<Throwable> = mock()
            val showLoadingObs: Observer<Boolean> = mock()

            viewModel.onDataLoaded().observeForever(resultObs)
            viewModel.getShowErrorLiveData().observeForever(errorObs)
            viewModel.getLoadingLiveData().observeForever(showLoadingObs)

            Mockito.`when`(surveyListUseCase.execute(1, 5)).thenReturn(Result.Error(mock()))

            viewModel.loadSurvey()

            val inOrder = Mockito.inOrder(showLoadingObs, showLoadingObs, errorObs)

            inOrder.verify(showLoadingObs).onChanged(true)
            inOrder.verify(showLoadingObs).onChanged(false)
            inOrder.verify(errorObs).onChanged(any())
            Mockito.verifyZeroInteractions(resultObs)
        }
    }

    @Test
    fun getSurveyList_onLoading_showLoadingView() {
        coroutineRule.runBlockingTest {

            val showLoadingObs: Observer<Boolean> = mock()

            viewModel.getLoadingLiveData().observeForever(showLoadingObs)


            viewModel.loadSurvey()

            Mockito.verify(showLoadingObs).onChanged(true)
        }
    }


    @Test
    fun onUserClicked_navigateToSurveyDetails() {
        val navigateObs: Observer<SurveyListModel.Item> = mock()
        val item: SurveyListModel.Item = mock()

        viewModel.getNavigateToDetails().observeForever(navigateObs)

        viewModel.onSurveyItemClick(item)

        Mockito.verify(navigateObs).onChanged(any())
    }

}