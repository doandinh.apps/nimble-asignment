package com.example.nimble.login

import androidx.lifecycle.Observer
import com.example.domain.Result
import com.example.domain.model.request.LoginRequest
import com.example.domain.usecase.LoginUseCase
import com.example.nimble.base.BaseViewModelTest
import com.example.nimble.rules.runBlockingTest
import com.example.nimble.util.UserManager
import com.example.nimble.viewmodel.LoginViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest : BaseViewModelTest() {

    @Mock
    lateinit var loginUseCase: LoginUseCase

    @Mock
    lateinit var userManager: UserManager

    private lateinit var viewModel: LoginViewModel

    @Before
    fun setUp() {
        viewModel = LoginViewModel(loginUseCase, userManager, coroutineRule.testDispatcherProvider)
    }

    @Test
    fun login_onSuccess_hideLoadingAndShowResult() {
        coroutineRule.runBlockingTest {
            val resultObs: Observer<Unit> = mock()
            val showLoadingObs: Observer<Boolean> = mock()
            viewModel.getNavigateToSurvey().observeForever(resultObs)
            viewModel.getLoadingLiveData().observeForever(showLoadingObs)


            viewModel.login("email", "password")

            val inOrder = Mockito.inOrder(showLoadingObs, resultObs)

            inOrder.verify(showLoadingObs).onChanged(true)
            Mockito.verifyZeroInteractions(resultObs)
        }
    }

    @Test
    fun login_onFailure_hideLoadingAndShowErrorMessage() {
        coroutineRule.runBlockingTest {

            val resultObs: Observer<Unit> = mock()
            val errorObs: Observer<Throwable> = mock()
            val showLoadingObs: Observer<Boolean> = mock()
            val request = LoginRequest(
                    "password",
                    "email",
                    "password"
            )

            viewModel.getNavigateToSurvey().observeForever(resultObs)
            viewModel.getShowErrorLiveData().observeForever(errorObs)
            viewModel.getLoadingLiveData().observeForever(showLoadingObs)

            Mockito.`when`(loginUseCase.execute(request)).thenReturn(Result.Error(mock()))

            viewModel.login("email", "password")

            val inOrder = Mockito.inOrder(showLoadingObs, showLoadingObs, errorObs)

            inOrder.verify(showLoadingObs).onChanged(true)
            inOrder.verify(showLoadingObs).onChanged(false)
            Mockito.verifyZeroInteractions(resultObs)
        }
    }

    @Test
    fun login_onLoading_showLoadingView() {
        coroutineRule.runBlockingTest {

            val showLoadingObs: Observer<Boolean> = mock()

            viewModel.getLoadingLiveData().observeForever(showLoadingObs)


            viewModel.login("test", "test")

            Mockito.verify(showLoadingObs).onChanged(true)
        }
    }


    @Test
    fun onLoginSuccess_navigateToSurveyList() {
        coroutineRule.runBlockingTest {
            val navigateObs: Observer<Unit> = mock()
            val request = LoginRequest(
                    "password",
                    "email",
                    "password"
            )
            viewModel.getNavigateToSurvey().observeForever(navigateObs)

            Mockito.`when`(loginUseCase.execute(request)).thenReturn(Result.Success(mock()))

            viewModel.login("email", "password")

            Mockito.verify(navigateObs).onChanged(any())
        }

    }

}