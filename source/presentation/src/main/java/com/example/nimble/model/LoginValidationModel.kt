package com.example.nimble.model

data class LoginValidationModel(
        var isEmailValid: Boolean, var isPasswordValid: Boolean
)