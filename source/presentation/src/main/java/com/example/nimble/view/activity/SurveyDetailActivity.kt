package com.example.nimble.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.nimble.R
import com.example.nimble.util.setStatusBarColor

class SurveyDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(ContextCompat.getColor(this, android.R.color.holo_green_dark))
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.survey_detail)
    }
}