package com.example.nimble.view.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.nimble.R
import com.example.nimble.base.BaseActivity
import com.example.nimble.databinding.ForgotPasswordLayoutBinding
import com.example.nimble.util.ErrorMsg
import com.example.nimble.util.gone
import com.example.nimble.util.setStatusBarColor
import com.example.nimble.util.visible
import com.example.nimble.viewmodel.ForgotPasswordViewModel


class ForgotPasswordActivity : BaseActivity() {


    private lateinit var binding: ForgotPasswordLayoutBinding
    private val viewModel by viewModels<ForgotPasswordViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(ContextCompat.getColor(this, android.R.color.holo_green_dark))
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.forgot_password)

        binding = DataBindingUtil.setContentView(this, R.layout.forgot_password_layout)
        binding.lifecycleOwner = this

        observeViewModel()
        setupViewListeners()
    }

    private fun setupViewListeners() {
        binding.btnSubmit.setOnClickListener {
            actionSubmit()

        }

        binding.edtEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.updateValue(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        binding.edtEmail.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                actionSubmit()
                return@OnEditorActionListener true
            }
            false
        })

    }

    private fun actionSubmit() {
        if (binding.progressBar.isGone && binding.btnSubmit.isEnabled) {
            val email = binding.edtEmail.text.toString()
            if (email.isNotEmpty()) {
                viewModel.submit(email)
            }
        }
    }

    private fun observeViewModel() {

        viewModel.getLoadingLiveData().observe { showLoading ->
            if (showLoading) {
                binding.progressBar.visible()
                binding.llLoginMain.gone()
            } else {
                binding.progressBar.gone()
                binding.llLoginMain.visible()
            }

        }


        viewModel.getResult().observe {
            binding.llLoginMain.gone()
            binding.tvResult.visible()
            binding.tvResult.text = it.message

        }

        viewModel.getShowErrorLiveData().observe { error ->
            Toast.makeText(
                    this,
                    ErrorMsg.getErrorMsg(ErrorMsg.RequestContext.FORGOT_PASSWORD, this, error),
                    Toast.LENGTH_LONG
            ).show()
        }

        viewModel.getEmailValidationLiveData()
                .observe { isEmailValid ->
                    binding.btnSubmit.isEnabled = isEmailValid
                    if (isEmailValid) {
                        binding.edtEmail.error = null

                    } else {
                        binding.edtEmail.error = getString(R.string.email_hint_err)
                    }
                }
    }

}