package com.example.nimble.util

import android.content.Context
import com.example.nimble.R
import retrofit2.HttpException


object ErrorMsg {
    fun getErrorMsg(request: RequestContext, context: Context, e: Throwable): String {
        return if (e is HttpException) {
            when (request) {
                RequestContext.LOGIN -> context.resources.getString(R.string.invalid_username_password)
                RequestContext.FORGOT_PASSWORD -> context.resources.getString(R.string.forgot_password_err)
                RequestContext.SURVEY_LIST -> context.resources.getString(R.string.get_survey_list_err)
            }
        } else {
            context.resources.getString(R.string.common_error)
        }
    }

    enum class RequestContext {
        LOGIN, FORGOT_PASSWORD, SURVEY_LIST
    }
}