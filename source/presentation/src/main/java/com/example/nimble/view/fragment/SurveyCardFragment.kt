package com.example.nimble.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.nimble.databinding.CardItemBinding

class SurveyCardFragment :
        Fragment() {

    companion object {
        private const val DATA_KEY = "DATA_KEY"
        fun instanceOf(
                item: String,
        ): SurveyCardFragment {
            val fragment = SurveyCardFragment()
            item.let {
                val args = Bundle()
                args.putString(DATA_KEY, it)
                fragment.arguments = args
            }

            return fragment
        }


    }

    private var cardItemBinding: CardItemBinding? = null
    private var data: String = ""


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        cardItemBinding = CardItemBinding.inflate(inflater, container, false)
        return cardItemBinding?.root!!
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        data = arguments?.getSerializable(DATA_KEY) as String

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        data.apply {
            if (isNotEmpty()) {
                cardItemBinding?.ivBackground?.let {
                    Glide.with(view.context).load("${this}l").into(it)
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        cardItemBinding = null
    }
}