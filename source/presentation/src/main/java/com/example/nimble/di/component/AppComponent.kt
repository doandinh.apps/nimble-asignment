package com.example.nimble.di.component

import android.app.Application
import com.example.nimble.base.BaseActivity
import com.example.nimble.base.BaseViewModel
import com.example.nimble.di.core.module.AppModule
import com.example.nimble.di.core.module.DataModule
import com.example.nimble.di.core.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, DataModule::class, NetworkModule::class])
interface AppComponent {

    fun inject(activity: BaseActivity)

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(application: Application): Builder
    }
}
