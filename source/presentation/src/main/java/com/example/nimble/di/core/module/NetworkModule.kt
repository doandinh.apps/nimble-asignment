package com.example.nimble.di.core.module

import com.example.data.api.AppService
import com.example.data.api.AuthenService
import com.example.domain.usecase.RefreshTokenUseCase
import com.example.nimble.BuildConfig
import com.example.nimble.util.RefreshTokenInterceptor
import com.example.nimble.util.UserManager
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(
            interceptor: HttpLoggingInterceptor,
            refreshTokenInterceptor: RefreshTokenInterceptor,
            builder: Retrofit.Builder
    ): Retrofit {
        val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(refreshTokenInterceptor)
                .addInterceptor(interceptor).build()
        return builder
                .client(client)
                .build()
    }

    @Provides
    fun provideLoggingInterceptor() =
            HttpLoggingInterceptor().apply {
                level =
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            }

    @Provides
    fun provideRefreshTokenInterceptor(
            refreshTokenUseCase: RefreshTokenUseCase,
            userManager: UserManager
    ): RefreshTokenInterceptor {
        return RefreshTokenInterceptor(refreshTokenUseCase, userManager)
    }


    @Singleton
    @Provides
    fun provideServiceApi(retrofit: Retrofit): AppService {
        return retrofit.create(AppService::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
                .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
    }

    @Singleton
    @Provides
    fun provideAuthenServiceApi(
            interceptor: HttpLoggingInterceptor,
            builder: Retrofit.Builder
    ): AuthenService {
        val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build()
        val retrofit = builder.client(client).build()
        return retrofit.create(AuthenService::class.java)
    }
}