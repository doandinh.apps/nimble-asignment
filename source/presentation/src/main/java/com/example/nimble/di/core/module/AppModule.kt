package com.example.nimble.di.core.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.example.data.AppDatabase
import com.example.nimble.di.annotation.ViewModelKey
import com.example.nimble.util.DispatchersProvider
import com.example.nimble.util.DispatchersProviderImpl
import com.example.nimble.viewmodel.ForgotPasswordViewModel
import com.example.nimble.viewmodel.LoginViewModel
import com.example.nimble.viewmodel.SurveyViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Provides
    @JvmStatic
    fun providesContext(application: Application): Context = application.applicationContext


    @Provides
    fun provideDispatchersProvider(): DispatchersProvider {
        return DispatchersProviderImpl()
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("NimbleExampleApp", Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideAppDataBase(context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

}

//@InstallIn(SingletonComponent::class)
//@Module
//abstract class AppModuleBinds {
//
//    @Binds
//    @IntoMap
//    @ViewModelKey(LoginViewModel::class)
//    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel
//
//    @Binds
//    @IntoMap
//    @ViewModelKey(ForgotPasswordViewModel::class)
//    abstract fun bindForgotPasswordViewModel(forgotPasswordViewModel: ForgotPasswordViewModel): ViewModel
//
//    @Binds
//    @IntoMap
//    @ViewModelKey(SurveyViewModel::class)
//    abstract fun bindSurveyViewModel(surveyViewModel: SurveyViewModel): ViewModel
//}