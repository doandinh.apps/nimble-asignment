package com.example.nimble.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.Result
import com.example.domain.model.ForgotPasswordModel
import com.example.domain.model.request.ForgotPasswordRequest
import com.example.domain.model.request.User
import com.example.domain.usecase.ForgotPasswordUseCase
import com.example.nimble.base.BaseViewModel
import com.example.nimble.util.DispatchersProvider
import com.example.nimble.util.isEmail
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ForgotPasswordViewModel @Inject constructor(
        private val forgotPasswordUseCase: ForgotPasswordUseCase,
        dispatchers: DispatchersProvider
) : BaseViewModel(dispatchers) {


    private val loadingLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    private val showErrorLiveData: MutableLiveData<Throwable> = MutableLiveData()
    private val showResult: MutableLiveData<ForgotPasswordModel> = MutableLiveData()
    private val emailValidationLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun submit(email: String) {
        if (loadingLiveData.value == false) {
            loadingLiveData.postValue(true)

            execute {
                val loginRequest = ForgotPasswordRequest(
                        user = User(email),
                )
                when (val result = forgotPasswordUseCase.execute(loginRequest)) {
                    is Result.Success -> {
                        loadingLiveData.postValue(false)
                        result.data.let {
                            showResult.postValue(it)
                        }

                    }

                    is Result.Error -> {
                        loadingLiveData.postValue(false)
                        showErrorLiveData.postValue(result.error)
                    }
                }
            }
        }

    }


    fun updateValue(email: String) {
        emailValidationLiveData.value = email.isEmail()
    }

    fun getLoadingLiveData(): LiveData<Boolean> = loadingLiveData
    fun getShowErrorLiveData(): LiveData<Throwable> = showErrorLiveData
    fun getResult(): LiveData<ForgotPasswordModel> = showResult
    fun getEmailValidationLiveData(): LiveData<Boolean> = emailValidationLiveData

}
