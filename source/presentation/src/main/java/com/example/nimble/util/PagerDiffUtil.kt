package com.example.nimble.util

import androidx.recyclerview.widget.DiffUtil
import com.example.domain.model.SurveyListModel

class PagerDiffUtil(
        private val oldList: List<SurveyListModel.Item>,
        private val newList: List<SurveyListModel.Item>
) : DiffUtil.Callback() {

    enum class PayloadKey {
        VALUE
    }

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id && areContentsTheSame(
                oldItemPosition,
                newItemPosition
        )
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].title == newList[newItemPosition].title && oldList[oldItemPosition].description == newList[newItemPosition].description && oldList[oldItemPosition].coverImageURL == newList[newItemPosition].coverImageURL
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any {
        return listOf(PayloadKey.VALUE)
    }
}
