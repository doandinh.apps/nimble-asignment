package com.example.nimble.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.Result
import com.example.domain.model.request.LoginRequest
import com.example.domain.usecase.LoginUseCase
import com.example.nimble.base.BaseViewModel
import com.example.nimble.model.LoginValidationModel
import com.example.nimble.util.DispatchersProvider
import com.example.nimble.util.UserManager
import com.example.nimble.util.isEmail
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
        private val loginUseCase: LoginUseCase,
        private val userManager: UserManager,
        dispatchers: DispatchersProvider
) : BaseViewModel(dispatchers) {

    private val loadingLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    private val animationEndLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    private val showErrorLiveData: MutableLiveData<Throwable> = MutableLiveData()
    private val navigateToSurvey: MutableLiveData<Unit> = MutableLiveData()
    private val loginLiveData: MutableLiveData<LoginValidationModel> = MutableLiveData()


    fun login(email: String, password: String) {
        if (loadingLiveData.value == false) {
            loadingLiveData.postValue(true)

            execute {
                val loginRequest = LoginRequest(
                        grantType = "password",
                        email = email,
                        password = password,
                )
                when (val result = loginUseCase.execute(loginRequest)) {
                    is Result.Success -> {
                        userManager.setUserModel(result.data)
                        navigateToSurvey.postValue(Unit)
                    }

                    is Result.Error -> {
                        loadingLiveData.postValue(false)
                        showErrorLiveData.postValue(result.error)
                    }
                }
            }
        }

    }

    fun updateValue(email: String, password: String) {
        loginLiveData.value = LoginValidationModel(
                isEmailValid = email.isEmail(),
                isPasswordValid = password.isNotEmpty()
        )
    }

    fun loginCheck() {
        userManager.getUserModel()?.let {
            navigateToSurvey.postValue(Unit)
        }
    }

    fun getLoadingLiveData(): LiveData<Boolean> = loadingLiveData
    fun getShowErrorLiveData(): LiveData<Throwable> = showErrorLiveData
    fun getNavigateToSurvey(): LiveData<Unit> = navigateToSurvey
    fun getLoginLiveData(): LiveData<LoginValidationModel> = loginLiveData
    fun getAnimationEndLiveData(): LiveData<Boolean> = animationEndLiveData

    fun updateAnimationStatus(isEnd: Boolean) {
        animationEndLiveData.postValue(isEnd)
    }

    fun isAnimationEnd(): Boolean {
        return animationEndLiveData.value ?: false
    }

}
