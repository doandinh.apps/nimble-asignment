package com.example.nimble.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.Result
import com.example.domain.model.SurveyListModel
import com.example.domain.usecase.GetSurveyListUseCase
import com.example.nimble.base.BaseViewModel
import com.example.nimble.util.DispatchersProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class SurveyViewModel @Inject constructor(
        private val surveyListUseCase: GetSurveyListUseCase,
        dispatchers: DispatchersProvider
) : BaseViewModel(dispatchers) {
    private val pageSize = 5
    private val surveyListLiveData: MutableLiveData<SurveyListModel> = MutableLiveData()
    private val loadingLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    private val noResult: MutableLiveData<Boolean> = MutableLiveData(false)
    private val showErrorLiveData: MutableLiveData<Throwable> = MutableLiveData()
    private val navigateToSurveyDetail: MutableLiveData<SurveyListModel.Item> = MutableLiveData()
    private val itemData: MutableLiveData<SurveyListModel.Item> = MutableLiveData()
    private var currentPage = 1
    private var isAbleToLoadMore = true
    private val simpleDateFormat by lazy { SimpleDateFormat("EEEE,  MMMM    dd", Locale.US) }

    fun loadSurvey() {
        if (isAbleToLoadMore && loadingLiveData.value == false) {
            loadingLiveData.postValue(true)

            execute {
                when (val localResult = surveyListUseCase.executeFromLocal(currentPage, pageSize)) {
                    is Result.Success -> {
                        Log.e("DB", "Record FOUND with SIzE : ${localResult.data.items.size}")
                        if (localResult.data.items.isNotEmpty()) {
                            localResult.data.fromRefresh = false
                            localResult.data.fromCache = true
                            loadingLiveData.postValue(false)
                            localResult.data.let {
                                surveyListLiveData.postValue(it)
                            }

                        }

                    }

                    is Result.Error -> {
                        Log.e("DB", "NO RECORD FOUND : ${localResult.error}")
                    }
                }
                when (val result = surveyListUseCase.execute(currentPage, pageSize)) {
                    is Result.Success -> {
                        result.data.fromRefresh = false
                        loadingLiveData.postValue(false)
                        result.data.let {
                            surveyListLiveData.postValue(it)
                        }

                        isAbleToLoadMore = currentPage < result.data.pages
                        if (currentPage == 1 && result.data.items.isNullOrEmpty()) {
                            noResult.value = true
                        }
                        if (isAbleToLoadMore) {
                            currentPage++
                        }

                    }

                    is Result.Error -> {
                        loadingLiveData.postValue(false)
                        showErrorLiveData.postValue(result.error)
                    }
                }
            }
        }

    }

    fun refresh() {
        if (loadingLiveData.value == false) {
            loadingLiveData.postValue(true)

            execute {
                when (val result = surveyListUseCase.execute(1, pageSize)) {
                    is Result.Success -> {
                        result.data.fromRefresh = true
                        currentPage = 1
                        loadingLiveData.postValue(false)
                        result.data.let {
                            surveyListLiveData.postValue(it)
                        }

                        isAbleToLoadMore = currentPage < result.data.pages
                        if (isAbleToLoadMore) {
                            currentPage++
                        }

                    }

                    is Result.Error -> {
                        loadingLiveData.postValue(false)
                        showErrorLiveData.postValue(result.error)
                    }
                }
            }
        }

    }


    fun onSurveyItemClick(survey: SurveyListModel.Item) {
        navigateToSurveyDetail.postValue(survey)
    }

    fun onDataLoaded(): LiveData<SurveyListModel> = surveyListLiveData
    fun getLoadingLiveData(): LiveData<Boolean> = loadingLiveData
    fun getShowErrorLiveData(): LiveData<Throwable> = showErrorLiveData
    fun getNavigateToDetails(): LiveData<SurveyListModel.Item> = navigateToSurveyDetail
    fun getNoResultState(): LiveData<Boolean> = noResult
    fun getCurrentItem(): LiveData<SurveyListModel.Item> = itemData

    fun getDateString(): String {
        return simpleDateFormat.format(Date())
    }

    fun updateItemData(item: SurveyListModel.Item) {
        itemData.value = item
    }
}
