package com.example.nimble.util

import android.content.SharedPreferences
import com.example.domain.model.UserModel
import com.example.nimble.AppConstants
import com.google.gson.Gson
import javax.inject.Inject

class UserManager @Inject constructor(
        private val sharedPreferences: SharedPreferences,
        private val gson: Gson
) {
    private var userModel: UserModel? = null

    fun getUserModel(): UserModel? {
        if (userModel == null && sharedPreferences.contains(AppConstants.USER_MODEL_KLEY)) {
            try {
                userModel = gson.fromJson(
                        sharedPreferences.getString(AppConstants.USER_MODEL_KLEY, ""),
                        UserModel::class.java
                )
            } catch (e: Exception) {
                e.printStackTrace()

            }

        }
        return userModel
    }

    fun setUserModel(data: UserModel?) {
        userModel = data
        if (userModel != null) {
            sharedPreferences.edit().putString(AppConstants.USER_MODEL_KLEY, gson.toJson(userModel))
                    .apply()
        } else {
            sharedPreferences.edit().remove(AppConstants.USER_MODEL_KLEY).apply()
        }
    }

}