package com.example.nimble.base

import android.graphics.Color
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import com.example.nimble.App
import com.example.nimble.util.setFullScreen
import com.example.nimble.util.setStatusBarColor
import com.example.nimble.viewmodel.ForgotPasswordViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
@AndroidEntryPoint
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStatusBarColor(Color.TRANSPARENT)
        setFullScreen()
    }

    fun <T> LiveData<T>.observe(observer: (T) -> Unit) {
        observe(this@BaseActivity, {
            it?.let { observer(it) }
        })
    }


}