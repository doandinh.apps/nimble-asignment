package com.example.nimble.di.core.module

import android.content.SharedPreferences
import com.example.data.AppDatabase
import com.example.data.api.AppService
import com.example.data.api.AuthenService
import com.example.data.repository.survey.SurveyDataSource
import com.example.data.repository.survey.SurveyLocalDataSource
import com.example.data.repository.survey.SurveyRemoteDataSource
import com.example.data.repository.survey.SurveyRepositoryImpl
import com.example.data.repository.user.UserDataSource
import com.example.data.repository.user.UserRemoteDataSource
import com.example.data.repository.user.UserRepositoryImpl
import com.example.domain.repository.SurveyRepository
import com.example.domain.repository.UserRepository
import com.example.domain.usecase.ForgotPasswordUseCase
import com.example.domain.usecase.GetSurveyListUseCase
import com.example.domain.usecase.LoginUseCase
import com.example.domain.usecase.RefreshTokenUseCase
import com.example.nimble.util.UserManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DataModule {

    @Provides
    @Singleton
    fun provideUserRepository(
            userRemote: UserDataSource
    ): UserRepository {
        return UserRepositoryImpl(userRemote)
    }

    @Provides
    @Singleton
    fun provideSurveyRepository(
            surveyRemote: SurveyDataSource,
            surveyLocal: SurveyLocalDataSource
    ): SurveyRepository {
        return SurveyRepositoryImpl(surveyRemote, surveyLocal)
    }

    @Provides
    @Singleton
    fun provideUserDataSource(serviceApi: AuthenService): UserDataSource {
        return UserRemoteDataSource(serviceApi)
    }

    @Provides
    @Singleton
    fun provideSurveyDataSource(serviceApi: AppService, db: AppDatabase): SurveyDataSource {
        return SurveyRemoteDataSource(serviceApi, db)
    }

    @Provides
    @Singleton
    fun provideSurveyLocalDataSource(db: AppDatabase): SurveyLocalDataSource {
        return SurveyLocalDataSource(db)
    }


    @Provides
    fun provideLoginUseCase(repository: UserRepository): LoginUseCase {
        return LoginUseCase(repository)
    }

    @Provides
    fun provideRefreshTokenUseCase(repository: UserRepository): RefreshTokenUseCase {
        return RefreshTokenUseCase(repository)
    }

    @Provides
    fun provideForgotPasswordUseCase(repository: UserRepository): ForgotPasswordUseCase {
        return ForgotPasswordUseCase(repository)
    }

    @Provides
    fun provideGetSurveyListUseCase(repository: SurveyRepository): GetSurveyListUseCase {
        return GetSurveyListUseCase(repository)
    }

    @Provides
    @Singleton
    fun provideUserManager(sharedPreferences: SharedPreferences, gson: Gson): UserManager {
        return UserManager(sharedPreferences, gson)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson = Gson()

}