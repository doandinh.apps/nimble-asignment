package com.example.nimble.util

import com.example.domain.Result
import com.example.domain.model.request.RefreshTokenRequest
import com.example.domain.usecase.RefreshTokenUseCase
import com.example.nimble.AppConstants
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class RefreshTokenInterceptor @Inject constructor(
        private val refreshTokenUseCase: RefreshTokenUseCase,
        private val userManager: UserManager
) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val authenticationRequest = request(originalRequest)
        val initialResponse = chain.proceed(authenticationRequest)

        when (initialResponse.code) {
            403, 401 -> {
                val userModel = userManager.getUserModel()
                userModel?.let { originUserModel ->
                    val request = RefreshTokenRequest(
                            "refresh_token",
                            originUserModel.refreshToken
                    )

                    when (val responseNewTokenLoginModel =
                            runBlocking { refreshTokenUseCase.execute(request) }) {
                        is Result.Success -> {
                            responseNewTokenLoginModel.data.let {
                                userManager.setUserModel(it)
                                val newAuthenticationRequest = originalRequest.newBuilder()
                                        .addHeader(
                                                AppConstants.AUTHORIZATION,
                                                "${it.tokenType} ${it.accessToken}"
                                        ).build()
                                return chain.proceed(newAuthenticationRequest)
                            }

                        }

                        is Result.Error -> {
                            return initialResponse
                        }
                        else -> return initialResponse
                    }

                } ?: return initialResponse

            }
            else -> return initialResponse
        }

    }

    private fun request(originalRequest: Request): Request {
        val builder = originalRequest.newBuilder()
        val userModel = userManager.getUserModel()
        userModel?.let {
            builder.addHeader(AppConstants.AUTHORIZATION, "${it.tokenType} ${it.accessToken}")
        }
        return builder.build()

    }
}