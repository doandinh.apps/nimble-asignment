package com.example.nimble.view.activity

import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.animation.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.nimble.R
import com.example.nimble.base.BaseActivity
import com.example.nimble.databinding.LoginLayoutBinding
import com.example.nimble.util.ErrorMsg
import com.example.nimble.util.hideSoftKeyboard
import com.example.nimble.viewmodel.ForgotPasswordViewModel
import com.example.nimble.viewmodel.LoginViewModel


class LoginActivity : BaseActivity() {

    private lateinit var binding: LoginLayoutBinding

    private val viewModel by viewModels<LoginViewModel>()
    private var isLoggedIn = false
    private val bgDrawable by lazy {
        ContextCompat.getDrawable(
                this@LoginActivity,
                R.drawable.login_blur_bg
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.login_layout)
        binding.lifecycleOwner = this
        binding.vm = viewModel as LoginViewModel
        observeViewModel()
        setupViewListeners()
        viewModel.loginCheck()
        startLogoAnimation()

    }

    private fun setupViewListeners() {
        binding.btnLogin.setOnClickListener {
            performLogin()
        }

        binding.tvForgot.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

        binding.edtEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.updateValue(
                        s.toString(),
                        binding.edtPassword.text.toString()
                )
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        binding.edtPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.updateValue(
                        binding.edtEmail.text.toString(),
                        s.toString()
                )
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })


        binding.edtPassword.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                performLogin()
                return@OnEditorActionListener true
            }
            false
        })

    }

    private fun performLogin() {
        if (binding.btnLogin.isEnabled) {
            binding.edtPassword.hideSoftKeyboard()
            val email = binding.edtEmail.text.toString()
            val password = binding.edtPassword.text.toString()
            viewModel.login(email, password)
        }
    }

    private fun showHideLoading(showLoading: Boolean) {
        if (showLoading) {
            binding.ivLogo.alpha = 0f
            binding.rootLogin.setBackgroundColor(
                    ContextCompat.getColor(
                            this,
                            android.R.color.black
                    )
            )
        } else {
            binding.ivLogo.alpha = 1f
            binding.rootLogin.background = bgDrawable
        }
    }

    private fun observeViewModel() {

        viewModel.getLoadingLiveData().observe { showLoading ->
            if (viewModel.isAnimationEnd()) {
                showHideLoading(showLoading)
            }

        }


        viewModel.getNavigateToSurvey().observe {
            isLoggedIn = true
            if (viewModel.isAnimationEnd()) {
                goToMain()
            }

        }

        viewModel.getShowErrorLiveData().observe { error ->
            Toast.makeText(
                    this,
                    ErrorMsg.getErrorMsg(ErrorMsg.RequestContext.LOGIN, this, error),
                    Toast.LENGTH_LONG
            ).show()
        }

        viewModel.getLoginLiveData().observe { loginValidationModel ->
            binding.btnLogin.isEnabled =
                    loginValidationModel.isEmailValid && loginValidationModel.isPasswordValid
            if (loginValidationModel.isEmailValid) {
                binding.edtEmail.error = null

            } else {
                binding.edtEmail.error = getString(R.string.email_hint_err)
            }
            if (loginValidationModel.isPasswordValid) {
                binding.edtPassword.error = null
            } else {
                binding.edtPassword.error = getString(R.string.password_hint_err)
            }
        }
    }

    private fun startLogoAnimation() {
        //slide animation
        val slide = TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, -8.0f
        )
        slide.duration = 1000
        slide.fillAfter = false
        slide.isFillEnabled = true
        slide.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                viewModel.updateAnimationStatus(true)
                if (isLoggedIn) {
                    goToMain()
                } else {
                    showHideLoading(false)
                }
            }
        })
        //
        val animSet = AnimationSet(true)
        animSet.fillAfter = true
        animSet.duration = 1000
        animSet.addAnimation(slide)
        val scale = ScaleAnimation(
                1f,
                .7f,
                1f,
                .7f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
        )
        scale.duration = 1000
        scale.fillAfter = true
        scale.isFillEnabled = true
        animSet.addAnimation(scale)


        //fade animation
        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator() //add this
        fadeIn.duration = 2000
        fadeIn.isFillEnabled = true
        fadeIn.fillAfter = true
        fadeIn.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                binding.rootLogin.background = bgDrawable

                binding.ivLogo.startAnimation(animSet)

            }
        })

        binding.ivLogo.animate().scaleX(2.0f).scaleY(2.0f)
                .setDuration(0)
                .withEndAction {
                    binding.ivLogo.startAnimation(fadeIn)
                }


    }

    private fun goToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }


}