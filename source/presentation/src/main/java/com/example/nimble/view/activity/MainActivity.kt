package com.example.nimble.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.nimble.R
import com.example.nimble.base.BaseActivity
import com.example.nimble.databinding.ActivityMainBinding
import com.example.nimble.util.ErrorMsg
import com.example.nimble.view.adapter.SurveyAdapter
import com.example.nimble.viewmodel.LoginViewModel
import com.example.nimble.viewmodel.SurveyViewModel
import com.google.android.material.tabs.TabLayoutMediator


class MainActivity : BaseActivity() {

    lateinit var surveyAdapter: SurveyAdapter
    lateinit var binding: ActivityMainBinding

    private val viewModel by viewModels<SurveyViewModel>()
    private val pageChangeCallback = object : OnPageChangeCallback() {

        override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
        ) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            if (position == surveyAdapter.itemCount - 1 && positionOffset == 0.toFloat() && positionOffsetPixels == 0) {
                viewModel.loadSurvey()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel 

        init()
        observeViewModel()
        viewModel.loadSurvey()

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun init() {
        initAdapter()
        binding.viewPager.registerOnPageChangeCallback(pageChangeCallback)

        binding.viewPager.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_MOVE -> binding.swipeRefreshLayout.isEnabled = false
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> binding.swipeRefreshLayout.isEnabled =
                        true
            }
            false
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    private fun initAdapter() {
        surveyAdapter = SurveyAdapter(this, arrayListOf())
        binding.viewPager.adapter = surveyAdapter
        TabLayoutMediator(binding.tlIndicator, binding.viewPager) { _, _ ->
        }.attach()

        binding.viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                surveyAdapter.getItem(position)?.let {
                    viewModel.updateItemData(it)
                }

            }
        })
    }


    private fun observeViewModel() {
        viewModel.getLoadingLiveData().observe { showLoading ->
            if (showLoading) {
                binding.swipeRefreshLayout.isRefreshing = false
            }

        }


        viewModel.onDataLoaded().observe { data ->
            binding.swipeRefreshLayout.isRefreshing = false
            if (data.fromRefresh) {
                initAdapter()
                surveyAdapter.setItems(data.items)
            } else {
                if (data.fromCache) {
                    surveyAdapter.addCachedItems(data.items)
                } else {
                    surveyAdapter.addItems(data.items)
                }

            }
        }

        viewModel.getNavigateToDetails().observe {
            val intent = Intent(this, SurveyDetailActivity::class.java)
            startActivity(intent)
        }

        viewModel.getShowErrorLiveData().observe { error ->
            binding.swipeRefreshLayout.isRefreshing = false
            Toast.makeText(
                    this,
                    ErrorMsg.getErrorMsg(ErrorMsg.RequestContext.SURVEY_LIST, this, error),
                    Toast.LENGTH_LONG
            ).show()
        }
    }


    override fun onDestroy() {
        binding.viewPager.unregisterOnPageChangeCallback(pageChangeCallback)
        super.onDestroy()
    }

}