package com.example.nimble

import android.app.Application
import com.example.nimble.di.component.AppComponent
import com.example.nimble.di.component.DaggerAppComponent
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {

}