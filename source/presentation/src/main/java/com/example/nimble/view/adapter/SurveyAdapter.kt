package com.example.nimble.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.domain.model.SurveyListModel
import com.example.nimble.util.PagerDiffUtil
import com.example.nimble.view.fragment.SurveyCardFragment


class SurveyAdapter(
        activity: FragmentActivity,
        private val items: MutableList<SurveyListModel.Item>,

        ) : FragmentStateAdapter(activity) {

    private var cachedItems: List<SurveyListModel.Item>? = null

    override fun getItemCount(): Int = items.size

    override fun createFragment(position: Int): Fragment =
            SurveyCardFragment.instanceOf(items[position].coverImageURL)

    fun addCachedItems(list: List<SurveyListModel.Item>) {
        if (list.isNotEmpty()) {
            processAddItems(list)
            cachedItems = list
        }

    }

    fun addItems(list: List<SurveyListModel.Item>) {
        cachedItems?.let {
            val callback = PagerDiffUtil(it, list)
            val diff = DiffUtil.calculateDiff(callback)
            items.removeAll(it)
            items.addAll(list)
            diff.dispatchUpdatesTo(this)
            cachedItems = null
        } ?: processAddItems(list)

    }


    private fun processAddItems(list: List<SurveyListModel.Item>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun setItems(list: List<SurveyListModel.Item>) {
        items.clear()
        processAddItems(list)
    }

    fun getItem(index: Int): SurveyListModel.Item? {
        val size = items.size
        if (index < size) {
            return items[index]
        }
        return null
    }


}