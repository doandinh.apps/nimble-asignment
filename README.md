# Nimble Assignment

- Architecture : Clean Architecture + MVVM
- Technical / Framework :
	+ Android Components : AndroidX, ViewBinding
	+ Network : Retrofit - OkHttp, REST Endpoint
	+ DI : Dagger 2
	+ DB : Room
	+ Concurrency : Coroutines
	+ Unit Test : JUnit, Mockito

- Flavors : 
	+ Production : use https://survey-api.nimblehq.co/ endpoint
	+ Staging : use https://nimble-survey-web-staging.herokuapp.com/ endpoint
	+ Mock : use test data as mock api

- Features : 
	+ Authentication:
		* Implement the login authentication screen.
		* Implement the OAuth authentication including the storage of access token.
		* Implement the automatic usage of refresh tokens to keep the user logged in using the OAuth API
		* Implement the forgot password authentication screen
	+ Home Screen:
		* Implement Survey List (with load more)
		* Implement Pull to Refresh
		* Implement Cache
	+ Survey Detail Screen (Blank screen)


